**目录说明**<br>
**python_examples**: python语言编写的对接demo代码<br/>
**samples**: 训练、推理使用的启动脚本、yaml配置demo文件<br/>
**yamls**: prometheus和calico启动的yaml配置demo文件<br/>
**dashboard_json**: grafana部署dashborad的json配置demo文件
**Directory description** <br/>
**python_examples**: demo docking code written in Python <br/>
**samples**: startup scripts used for training and inference, and YAML configuration demo files<br/>
**yamls**: YAML configuration demo files launched by Prometheus and calico
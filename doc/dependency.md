# ubuntu

python3-pip

# ansible

ansible==4.7.0
ansible-core==2.11.6
MarkupSafe=2.0.1
Jinja2==3.0.1
pip==21.1
certifi==2018.1.18
cryptography==2.1.4
pyyaml==3.12
